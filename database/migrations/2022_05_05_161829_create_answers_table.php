<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('answers', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
        });

        $answers = [
            ['name'=>'Да.'],
            ['name'=>'Нет.'],
            ['name'=>'Возможно.'],
            ['name'=>'Вопрос не ясен.'],
            ['name'=>'Абсолютно точно.'],
            ['name'=>'Никогда.'],
            ['name'=>'Даже не думай.'],
            ['name'=>'Сконцентрируйся и спроси опять.'],
        ];
        DB::table('answers')->insert($answers);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('answers');
    }
}
