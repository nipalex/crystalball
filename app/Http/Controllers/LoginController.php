<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    const DEFAULT_NAME = '';
    const DEFAULT_PASSWORD = '111';

    public function index()
    {
        return view('login');
    }

    public function authenticate(Request $request)
    {
        $user = User::where('email', $request->input('email'))->first();
        if (!$user) {
            $this->validate($request, [
                'email' => 'required|string',
            ]);

            $user = User::create([
                'name' => self::DEFAULT_NAME,
                'email' => $request->input('email'),
                'password' => self::DEFAULT_PASSWORD,
            ]);
        }
        Auth::login($user);

        return redirect()->intended('question');
    }

}
