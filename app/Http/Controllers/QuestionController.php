<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use App\Models\Question;
use Illuminate\Http\Request;

class QuestionController extends Controller
{

    public function index()
    {
        return view('question');
    }

    public function answer(Request $request)
    {
        $this->validate($request, [
            'question' => 'required|string',
        ]);

        $answerCounter = 1;
        $answer = Answer::inRandomOrder()->first();
        $question = Question::where('name', $request->input('question'))->first();

        if ($question) {
            $question->increment('counter');
            $answerCounter = $question->counter;
        } else {
            $question = new Question;
            $question->name = $request->input('question');
            $question->save();
        }

        return view('question', compact('answerCounter','answer'));
    }
}
