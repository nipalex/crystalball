@extends('layouts.app', ['title' => 'Авторизация'])
@section('content')
    <form method="POST" action="/login" style="text-align: center; margin: auto;">
        {!! csrf_field() !!}
        @php $user = \Illuminate\Support\Facades\Auth::user();
        if (isset($user)) {
            echo $user->id;
        }
        @endphp
        <div>
            Имя <input style="margin: 10px;" type="text" required name="email" value="{{ old('email') }}">
        </div>

        <div>
            <button style="margin: 10px;" type="submit">Вход</button>
        </div>
    </form>
@endsection
